import axios from 'axios'; 

const api = axios.create({ 
    baseURL: 'https://delivery-center.herokuapp.com/', 
}); 

 export default api; 
import React, { createContext } from 'react'
import api from '../services/api';

const orders22 = [
    {dia: 6, mes: 'set', ref: 'REF#124', cliente: 'Nome sobrenome', total: 42342, id: 1, empresa: 'DPIZZA' },
    {dia: 6, mes: 'set', ref: 'REF#124', cliente: 'Nome sobrenome', total: 42342, id: 2, empresa: 'DPIZZA' },
    {dia: 6, mes: 'set', ref: 'REF#124', cliente: 'Nome sobrenome', total: 42342, id: 3, empresa: 'DPIZZA' }
]
const OrdersContext = createContext({})


export const OrdersProvider = props => {
        const [orders, setCurrentUser] = React.useState(orders22);

        return (
            <OrdersContext.Provider value={{
                state: {
                    orders
                }
            }}>
                {props.children}
            </OrdersContext.Provider>
        )
}

export default OrdersContext;
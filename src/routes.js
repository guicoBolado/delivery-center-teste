import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

import Login from './pages/login/Login';
import Home from './pages/home/Home';
import Navbar from './components/Navbar';
import Order from './pages/order/Order';
import Tabs from './components/Tabs';
import OrderDetails from './pages/order/OrderDetails';

export default function Routes(){
    return (
        <BrowserRouter>
            <Switch>
                <Redirect exact from="/" to="/login" />
                <Route path="/login" exact component={Login}>
                    <Login />
                </Route>

                <Route path="/home" exact component={Home}>
                    <Navbar />
                    <Tabs />
                    <Home />
                </Route>
                <Route path="/order" exact component={Order}>
                    <Navbar />
                    <Tabs />
                    <Order />
                </Route>     
                <Route path="/order/:id" exact component={OrderDetails}>
                    <Navbar />
                    <Tabs />
                    <OrderDetails />
                </Route>                             
            </Switch>
        </BrowserRouter>
    )

}
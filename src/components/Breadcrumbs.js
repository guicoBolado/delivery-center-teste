import React, { Component } from 'react';
import Typography from '@material-ui/core/Typography';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';


class breadcrumb  extends Component {
    render(){

        const { itens, active } = this.props;
        
        return (
            <Breadcrumbs aria-label="breadcrumb" style={{marginBottom: 30, marginTop: 30}}>
                {itens.map(itens => <Link color="inherit" key={itens.link} href={itens.link}>{itens.title}</Link>)}                
                <Typography color="textPrimary">{active}</Typography>
            </Breadcrumbs>
        );
    }
}
export default breadcrumb;
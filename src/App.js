import React from 'react';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import { OrdersProvider } from './context/OrdersContext';

import './App.css';
import './styles/colors'

import Routes from './routes';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#FE5200',
    },
    secondary: {
      main: '#f44336',
    },
  },
});


function App() {

  return (
    <OrdersProvider>
      <ThemeProvider theme={theme}>
        <Routes/>       
      </ThemeProvider>
    </OrdersProvider>
  );
}

export default App;

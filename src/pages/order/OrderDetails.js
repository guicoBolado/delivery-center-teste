import React, { useEffect } from 'react';

import {  Box, 
          Button, 
          Container, 
          fade, 
          Grid, 
          List, 
          makeStyles, 
          Paper, 
          Typography, 
          ListItemText, 
          Divider } from '@material-ui/core';
import PersonOutlineOutlinedIcon from '@material-ui/icons/PersonOutlineOutlined';
import AttachMoneyOutlinedIcon from '@material-ui/icons/AttachMoneyOutlined';
import Breadcrumbs from '../../components/Breadcrumbs';
import { useHistory, useParams } from 'react-router-dom';
import ArrowBackOutlinedIcon from '@material-ui/icons/ArrowBackOutlined';
import DateRangeOutlinedIcon from '@material-ui/icons/DateRangeOutlined';
import StorefrontOutlinedIcon from '@material-ui/icons/StorefrontOutlined';
import RoomOutlinedIcon from '@material-ui/icons/RoomOutlined';
import api from '../../services/api';


const useStyles = makeStyles((theme) => ({
    paper: {
        padding: 30,
        backgroundColor: 'white',
    },
    subtitle: {
        fontSize: 12,
        color: 'grey'
    },
    title: {
        fontSize: 18,
    },
    iconTitle: {
        fontSize: 28,
    },
    boxDate: {
        backgroundColor: fade(theme.palette.primary.main, 0.3),
        width: 55,
        height: 55,
        textAlign: 'center',
        borderRadius: 10
    },
    day: {
        fontSize: 25,
    },
    month: {
        fontSize: 15,
        lineHeight: 0.2
    },
    box : {
      marginBottom: 20
    }


}));

export default function OrderDetails() {
  const classes = useStyles();
    const itensBreadcrumbs = [
        {title: 'Início', link: '/home'},
        {title: 'Pedidos', link: '/order'}
    ];

    const [orders, setCurrentUser] = React.useState([]);

    let { id } = useParams();
    
    useEffect(() => { 
        async function getUser() {
          try {
            const response = await api.get(`order/${id}`);
            setCurrentUser(response.data)
          } catch (error) {
              console.error(error);
            }
          }
          getUser()
        }, []);



  useEffect(() => {
   
  }, [])
  const history = useHistory();

  return (
    <Container>
        <Breadcrumbs itens={itensBreadcrumbs} active={'Detalhes do pedido'}/>
        <Grid container spacing={0}>
            <Grid item sm={6} xs={12} className={classes.box}>
                <Typography variant="h4">
                    Pedido {id}
                </Typography>
                <Typography variant="h6">
                    Detalhes do pedido REF: #123
                </Typography>
            </Grid>
            <Grid item sm={6} xs={12} style={{textAlign: 'right'}}>
              <Button variant="outlined" color="primary" onClick={() => history.push('/order')}>
                <ArrowBackOutlinedIcon /> VOLTAR
              </Button>
            </Grid>            
        </Grid>

        <Grid container spacing={3}>
              <Grid item sm={6} xs={12} >
                  <Paper className={classes.paper}>
                    <Typography variant="h5" color="primary" className={classes.box}>
                      Pedido
                    </Typography>
                    <Box display="flex" flexDirection="column" className={classes.box}>
                        <Typography variant="h6" className={classes.subtitle}>
                            Data de cadastro
                        </Typography>
                        <Box display="flex">
                            <DateRangeOutlinedIcon className={classes.iconTitle} color="primary"/> 
                            <Typography variant="h6" className={classes.title}>10/01/2020 - 15:34 </Typography> 
                        </Box>                            
                    </Box>        
                    <Box display="flex" flexDirection="column" className={classes.box}>
                        <Typography variant="h6" className={classes.subtitle}>
                            Lojista
                        </Typography>
                        <Box display="flex">
                            <StorefrontOutlinedIcon className={classes.iconTitle} color="primary"/> 
                            <Typography variant="h6" className={classes.title}>{orders.store}</Typography> 
                        </Box>                            
                    </Box>   
                    <Box display="flex" flexDirection="column" className={classes.box}>
                        <Typography variant="h6" className={classes.subtitle}>
                            Itens do pedido
                        </Typography>
                        <List component="nav" aria-label="secondary mailbox folders">
                          <ListItemText primary="Pizza" secondary="R$4242424"/>
                          <Divider />
                          <ListItemText primary="Suco" secondary="R$4242424"/>
                          <Divider />
                        </List>    

                    </Box>  
                    <Box display="flex" flexDirection="row" justifyContent="space-between" className={classes.box}>
                      <Box  display="flex" flexDirection="row">

                        <AttachMoneyOutlinedIcon className={classes.iconTitle} color="primary"/> 
                        <Typography variant="h6">
                            Total
                        </Typography>
                      </Box>
                        <Box display="flex" alignItems="flexEnd">
                            <Typography variant="h6" className={classes.title}>R$ 8.980,00</Typography> 
                        </Box>                            
                    </Box>                                                                            
                    
                  </Paper>
              </Grid>

              <Grid item sm={6} xs={12} >
                  <Paper className={classes.paper}>
                    <Box style={{marginBottom: 40}}>
                      <Typography variant="h5" color="primary" className={classes.box}>
                        Cliente
                      </Typography>
                      <Box display="flex" flexDirection="column" className={classes.box}>
                          <Typography variant="h6" className={classes.subtitle}>
                              Nome
                          </Typography>
                          <Box display="flex">
                              <PersonOutlineOutlinedIcon className={classes.iconTitle} color="primary"/> 
                              <Typography variant="h6" className={classes.title}>Kayo</Typography> 
                          </Box>                            
                      </Box> 
                      <Box display="flex" flexDirection="column" className={classes.box}>
                          <Typography variant="h6" className={classes.subtitle}>
                              Endereço
                          </Typography>
                          <Box display="flex">
                              <RoomOutlinedIcon className={classes.iconTitle} color="primary"/> 
                              <Typography variant="h6" className={classes.title}>Rua dos Pinheiros </Typography> 
                          </Box>                            
                      </Box> 
                    </Box>
                    <Box>
                      <Typography variant="h5" color="primary" className={classes.box}>
                        Pagamento
                      </Typography>
                      <Box display="flex" flexDirection="column" className={classes.box}>
                          <Typography variant="h6" className={classes.subtitle}>
                              Método
                          </Typography>
                          <Box display="flex">
                              <PersonOutlineOutlinedIcon className={classes.iconTitle} color="primary"/> 
                              <Typography variant="h6" className={classes.title}>CREDIT</Typography> 
                          </Box>                            
                      </Box> 
                      <Box display="flex" flexDirection="column" className={classes.box}>
                          <Typography variant="h6" className={classes.subtitle}>
                              Valor de pagamento
                          </Typography>
                          <Box display="flex">
                              <RoomOutlinedIcon className={classes.iconTitle} color="primary"/> 
                              <Typography variant="h6" className={classes.title}>34242 </Typography> 
                          </Box>                            
                      </Box> 
                    </Box>                    
                    
                  
                  </Paper>
              </Grid>
        </Grid>
        <Button variant="outlined" color="primary" onClick={() => history.push('/order')} style={{marginTop: 50}}>
          <ArrowBackOutlinedIcon /> VOLTAR
        </Button>
        
    </Container>
  );
}
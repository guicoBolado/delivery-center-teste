import React, { useContext, useEffect } from 'react';

import { Box, Button, Container, fade, Grid, makeStyles, Menu, MenuItem, Paper, Typography } from '@material-ui/core';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import PersonOutlineOutlinedIcon from '@material-ui/icons/PersonOutlineOutlined';
import AttachMoneyOutlinedIcon from '@material-ui/icons/AttachMoneyOutlined';
import Breadcrumbs from '../../components/Breadcrumbs';
import { useHistory } from 'react-router-dom';
import OrdersContext from '../../context/OrdersContext';
import api from '../../services/api';




const useStyles = makeStyles((theme) => ({
    paper: {
        padding: 30,
        backgroundColor: 'white',
        cursor: 'pointer'
    },
    subtitle: {
        fontSize: 12,
        color: 'grey'
    },
    title: {
        fontSize: 18,
    },
    iconTitle: {
        fontSize: 28,
    },
    boxDate: {
        backgroundColor: fade(theme.palette.primary.main, 0.3),
        width: 55,
        height: 55,
        textAlign: 'center',
        borderRadius: 10
    },
    day: {
        fontSize: 25,
    },
    month: {
        fontSize: 15,
        lineHeight: 0.2
    }


}));


export  default function Order() {

    const { state } = useContext(OrdersContext)
    const [orders, setCurrentUser] = React.useState([]);

  const classes = useStyles();
    const itensBreadcrumbs = [
        {title: 'Início', link: '/home'}
    ];

    useEffect(() => { 
        async function getUser() {
          try {
            const response = await api.get('order');
            setCurrentUser(response.data)
          } catch (error) {
              console.error(error);
            }
          }
          getUser()
        }, []); 

  const history = useHistory();

  const [anchorEl, setAnchorEl] = React.useState(null);
 
  const handleClick = (event) => {
      setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
   };

  return (
    <Container>
        <Breadcrumbs itens={itensBreadcrumbs} active={'Pedidos'}/>
        <Grid container spacing={0}>
            <Grid item sm={6} xs={12} >
                <Typography variant="h4">
                    Pedidos 
                </Typography>
                <Typography variant="h6">
                    Seus pedidos recebidos até o momento.
                </Typography>
            </Grid>
            <Grid item sm={6} xs={12}   style={{textAlign: 'right'}}>
                <Button variant="outlined" color="primary" aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}  style={{marginRight: 20}}>
                    Ordem
                </Button>
                <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={handleClose}
                >
                    <MenuItem onClick={handleClose}>Data de cadastramento</MenuItem>
                    <MenuItem onClick={handleClose}>Valor do pedido</MenuItem>
                </Menu>    

                <Button variant="contained" color="primary">
                    CADASTRAR
                </Button>            
            </Grid>            
        </Grid>

        <Grid container spacing={3}>

            {orders.map(orders => 
                <Grid item sm={6} xs={12}  key={orders._id}>
                    <Paper className={classes.paper} onClick={() => history.push(`/order/${orders._id}`)}>
                        <Box display="flex" flexDirection="row" justifyContent="left" style={{marginBottom: 30, paddingLeft: 100}}>
                            <Box display="flex" flexDirection="column" className={classes.boxDate}>
                                <Typography variant="h6" color="primary" className={classes.day}>
                                    <strong>06</strong>
                                </Typography>
                                <Typography variant="h6" className={classes.month}>
                                    set
                                </Typography>                          
                            </Box>  

                            <Box display="flex" flexDirection="column" style={{marginLeft: 20}}>
                                <Typography variant="h6" className={classes.subtitle}>
                                    #REF123
                                </Typography>
                                <Box display="flex">
                                    <Typography variant="h6" className={classes.title}><strong>{orders.store}</strong></Typography> 
                                </Box>                            
                            </Box> 
                        </Box>

                        <Box display="flex" flexDirection="row" justifyContent="center" style={{marginBottom: 30}}>
                            <Box display="flex" flexDirection="column" >
                                <Typography variant="h6" className={classes.subtitle}>
                                    Cliente: 
                                </Typography>
                                <Box display="flex">
                                    <PersonOutlineOutlinedIcon className={classes.iconTitle} color="primary"/> 
                                    <Typography variant="h6" className={classes.title}>{orders.customer[0].name} </Typography> 
                                </Box>                            
                            </Box>  

                            <Box display="flex" flexDirection="column" style={{marginLeft: 20}}>
                                <Typography variant="h6" className={classes.subtitle}>
                                    Total com frete: 
                                </Typography>
                                <Box display="flex">
                                    <AttachMoneyOutlinedIcon className={classes.iconTitle} color="primary"/> 
                                    <Typography variant="h6" className={classes.title}>R$ {orders.payments[0].amount} </Typography> 
                                </Box>                            
                            </Box> 
                        </Box>
                        <Box justifyContent="center" display="flex">
                            <Button  variant="outlined" color="primary">
                                VER DETALHES <ArrowForwardIcon/>
                            </Button>
                        </Box>
                    
                    </Paper>
                </Grid>
            )}   
        </Grid>
        
    </Container>
  );
}
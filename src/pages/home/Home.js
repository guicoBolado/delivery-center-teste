import React, { useEffect } from 'react';

import { Box, Button, Container, fade, makeStyles, Typography } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow'
import ShoppingCartOutlinedIcon from '@material-ui/icons/ShoppingCartOutlined';
import PeopleAltOutlinedIcon from '@material-ui/icons/PeopleAltOutlined';
import AttachMoneyOutlinedIcon from '@material-ui/icons/AttachMoneyOutlined';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';


import Chart from 'chart.js';
import { Link } from 'react-router-dom';

import { useQuery, useMutation } from '@apollo/react-hooks';
import gql from "graphql-tag";
import api from '../../services/api';


const READ_TODOS = gql`
  query getOrders{
    orders {
      _id
      store
      externalReference
      reference
      amount
      customer {
        _id
        name
      }
      name
    }
  }
`;



const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: 30,
    color: theme.palette.text.secondary,
    border: 1,
    borderColor: 'red',
    display: 'flex',
  },
  flexColumn: {
    flexDirection: 'column',
    padding: 20
  },
  boxIcon:{
    fontSize: 80,
    color: 'red',
    width: '40%',
  },
  boxText: {
    fontSize: 25,
    display: 'flex',
    flexDirection: 'column',
    textAlign: 'right',
    width: '60%',
    paddingRight: 30,
    color: 'black'

  },
  boxDate: {
    backgroundColor: fade(theme.palette.primary.main, 0.3),
    width: 55,
    height: 55,
    textAlign: 'center',
    borderRadius: 10
  },
  day: {
      fontSize: 25,
  },
  month: {
      fontSize: 15,
      lineHeight: 0.2
  }

}));

export default function Home() {
  const classes = useStyles();

  const [orders, setCurrentUser] = React.useState([]);


  const { data, loading, error } = useQuery(READ_TODOS);
  // if (loading) return <p>loading...</p>;
  // // if (error) return <p>ERROR</p>;
  // // if (!data) return <p>Not found</p>;


  useEffect(() => {
    async function getUser() {
      try {
        const response = await api.get('order');
        setCurrentUser(response.data)
        var stores = response.data.map(dados => dados.store);
        var ctx = document.getElementById("myChart");
        // eslint-disable-next-line
    
        console.log(stores.filter(x => x === 'DPIZZA').length)

        var lineChart = new Chart(ctx, {
          type: 'doughnut',
          data: {
            labels: stores,
            datasets: [
              {
                label: "Population (millions)",
                backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                data: [1,1,1,1]
              }
            ]
          },
        })
      } catch (error) {
          console.error(error);
        }
      }
    getUser()
  }, [])

  return (
    <Container>
      <h1>Olá Nome Sobrenome</h1>
      <h3>Acompanhe o resumo geral das atividades da sua empresa.</h3>
      <Grid container spacing={3}>

        <Grid item sm={4} xs={12} >
          <Paper className={classes.paper}>
            <ShoppingCartOutlinedIcon className={classes.boxIcon}/>
            <div className={classes.boxText}>
              <strong>{orders.length}</strong>
              Pedidos
            </div>
          </Paper>
        </Grid>

        <Grid item sm={4} xs={12}>
          <Paper className={classes.paper}>
            <PeopleAltOutlinedIcon className={classes.boxIcon}/>
            <div className={classes.boxText}>
              <strong>3</strong>
              Clientes
            </div>
          </Paper>
        </Grid>

        <Grid item sm={4} xs={12}>
          <Paper className={classes.paper}>
            <AttachMoneyOutlinedIcon className={classes.boxIcon}/>
            <div className={classes.boxText}>
              <strong>R$ 34.231,00</strong>
              Faturamento
            </div>
          </Paper>
        </Grid>


        <Grid item sm={6} xs={12} >
          <Paper className={classes.paper, classes.flexColumn}>
            <Typography variant="h5"  color='primary'>
              Pedidos recentes
            </Typography>
              <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="simple table">
                  <TableBody>
                    {orders.map((row) => (
                      <TableRow key={row._id}>
                        <TableCell component="th" scope="row">
                          {row.name}
                        </TableCell>
                        <TableCell className={classes.tableDate} align="center">

                          <Box display="flex" flexDirection="column" className={classes.boxDate}>
                              <Typography variant="h6" color="primary" className={classes.day}>
                                  <strong>6</strong>
                              </Typography>
                              <Typography variant="h6" className={classes.month}>
                                  Set
                              </Typography>                          
                          </Box>  
                        </TableCell>

                        <TableCell align="left">
                          <Typography variant="h6">
                            #REF123
                          </Typography>
                          <Typography variant="h6">
                            <strong>{row.store}</strong>
                          </Typography>                          
                           
                        </TableCell>

                        <TableCell align="right">
                          <Link to={`/order/${row.id}`}>
                            <Button variant="outlined" size="large" color="primary" className={classes.margin}>
                              <ArrowForwardIcon />
                            </Button>

                          </Link>
                          
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>            
            
          </Paper>
        </Grid>

        <Grid item sm={6} xs={12} >
          <Paper className={classes.paper, classes.flexColumn}>
            <Typography variant="h5"  color='primary'>
              Pedidos por lojistas
            </Typography>
            <canvas id="myChart" width="400" height="225"></canvas>
          </Paper>
        </Grid>
   
      </Grid>
    </Container>
  );
}
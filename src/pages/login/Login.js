import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { useHistory } from "react-router-dom";

import bg from '../../assets/bg.jpg';
import logo from '../../assets/logo.png';

export default function Login() {

    const history = useHistory();

    useEffect(() => {
        if (localStorage.getItem('user')) history.push('/home')
        // eslint-disable-next-line
    }, [])


  const classes = useStyles();
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')


  async function clickSubmit(event) {
    console.log('event', event);
    console.log('email', email);
    console.log('password', password);
    
    localStorage.setItem('user', email)
    history.push('/home')
  }


  return (
    <Container component="main" maxWidth="xl" className={classes.bg}>
      <CssBaseline />
      <div className={classes.paper}>
        <img src={logo} />
        <Typography component="h1" variant="h5">
          Entrar
        </Typography>
        <form className={classes.form} onSubmit={clickSubmit}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="E-mail"
            name="email"
            autoComplete="email"
            type="email"
            autoFocus
            value={email}
            onChange={event => setEmail(event.target.value)}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Senha"
            type="password"
            id="password" 
            value={password}
            onChange={event => setPassword(event.target.value)}
          />
        
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Entrar
          </Button>
         
        </form>
      </div>

    </Container>
  );
}


const useStyles = makeStyles((theme) => ({
    paper: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      height: 400,
      justifyContent: 'center',
      maxWidth: 400,
      backgroundColor: 'white',
      padding: 50
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', 
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
    bg : {
      backgroundImage:`url(${bg})`,
      backgroundPosition: 'center',
      backgroundSize: 'cover',
      backgroundRepeat: 'no-repeat',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      minHeight: '100vh'
    }
  }));